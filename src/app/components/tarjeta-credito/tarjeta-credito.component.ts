import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Subscriber } from 'rxjs';
import { TarjetaService } from 'src/app/services/tarjeta.service';
import { NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'app-tarjeta-credito',
  templateUrl: './tarjeta-credito.component.html',
  styleUrls: ['./tarjeta-credito.component.css']
})

export class TarjetaCreditoComponent implements OnInit {

  listTarjetas: any[] = [];
  accion = 'Agregar';
  form: FormGroup;
  id: number | undefined;

  constructor( 
    private fb: FormBuilder, 
    private toastr: ToastrService,
    private spinner: NgxSpinnerService,
    private _tarjetaService: TarjetaService) {
    
    this.form = this.fb.group({
      titular: ['', Validators.required],
      numeroTarjeta: ['', [Validators.required, Validators.maxLength(16), Validators.minLength(16)]],
      fechaExpiracion: ['', [Validators.required, Validators.maxLength(5), Validators.minLength(5)]],
      cvv: ['', [Validators.required, Validators.maxLength(3), Validators.minLength(3)]]
    });

  }

  
  ngOnInit(): void {    

    this.obtenerTarjetas();

  }

  obtenerTarjetas() {

    this.spinner.show();

    this._tarjetaService.getListaTarjetas().subscribe(data => {
                  
      this.listTarjetas = data;   

      this.spinner.hide();
    }, error => {
      console.log(error);
      this.spinner.hide();
    });
    
  }

  guardarTarjeta() {

    this.spinner.show();

    const d = new Date();

    const tarjeta : any = {
      titular : this.form.get('titular')?.value,
      numeroTarjeta : this.form.get('numeroTarjeta')?.value,
      fechaExpiracion : this.form.get('fechaExpiracion')?.value,
      cvv : this.form.get('cvv')?.value,
      fechaRegistro :  new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString().split('.')[0].replace('T',' ')

    }
    
    if (this.id == undefined) {
      
      // agregar nueva tarjeta      
      this._tarjetaService.saveTarjeta(tarjeta).subscribe(data => {

        this.toastr.success('La tarjeta fue registrada con exito', 'Tarjeta registrada');
        this.obtenerTarjetas();
        this.form.reset();
  
        this.spinner.hide();
      }, error => {
        console.log(error);
        this.toastr.error('Ocurrio un error','Error');
        this.spinner.hide();
      });
                
    } else {
      
      tarjeta.id = this.id;

      // editar tarjeta
      this._tarjetaService.updateTarjeta(this.id, tarjeta).subscribe(data => {
        
        this.form.reset();
        this.accion = 'Agregar';
        this.id = undefined;
        this.toastr.info('La tarjeta fue actualizada correctamente', 'Tarjeta actualizada');
        this.obtenerTarjetas();
        this.spinner.hide();

      }, error => {
        console.log(error);
        this.toastr.error('Ocurrio un error','Error');
        this.spinner.hide();
      });

    }
    
  }

  eliminarTarjeta(id: number) {
            
    this.spinner.show();

    this._tarjetaService.deleteTarjeta(id).subscribe(data => {
      this.toastr.error("La tarjeta fue eliminada con exito", "Tarjeta eliminada");
      this.obtenerTarjetas();
      this.spinner.hide();
    }, error => {
      console.log(error);    
      this.spinner.hide();
    });
    
  }

  editarTarjeta(tarjeta: any) {

    
    this.accion = "Editar";
    this.id = tarjeta.id;

    this.form.patchValue({
      titular: tarjeta.titular,
      numeroTarjeta: tarjeta.numeroTarjeta,
      fechaExpiracion: tarjeta.fechaExpiracion,
      cvv: tarjeta.cvv,
      fechaRegistro :  new Date(new Date().toString().split('GMT')[0]+' UTC').toISOString().split('.')[0].replace('T',' ')
    });
      
  }

}

